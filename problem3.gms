*Fleet Planning

set
v set of ships /v1*v5/
r set of routes /Asia, ChinaPacific/
p set of ports /Singapore, Incheon, Shanghai, Sydney, Gladstone, Dalian, Osaka, Victoria/
h1(p) subset of incompatible port pairs /Singapore, Osaka/
h2(p) subset of incompatible port pairs /Incheon, Victoria/
;

parameter
F(v) fixed cost in million dollars if ship v is used /v1 65, v2 60, v3 92, v4 100, v5 110/
G(v) number of days ship v can sail in a year /v1 300, v2 250, v3 350, v4 330, v5 300/
D(p) minimum number of times a port must be visited /Singapore 15, Incheon 18, Shanghai 32, Sydney 32, Gladstone 45, Dalian 32, Osaka 15, Victoria 18/
;

Table A(p,r) 1 if route r passes through port p and 0 otherwise
            Asia   ChinaPacific
Singapore      1              0
Incheon        1              0
Shanghai       1              1
Sydney         0              1
Gladstone      0              1
Dalian         1              1
Osaka          1              0
Victoria       0              1
;

Table C(v,r)  the cost of completing one time route r in million dollars
    Asia  ChinaPacific
v1  1.41           1.9
v2   3.0           1.5
v3   0.4           0.8
v4   0.5           0.7
v5   0.7           0.8
;

Table T(v,r)  the number of days needed to complete one time route r
    Asia  ChinaPacific
v1  14.4          21.2
v2  13.0          20.7
v3  14.4          20.6
v4  13.0          19.2
v5  12.0          20.1
;

scalar
k the company has to service at least k ports
;
k=5

*I define the variables 
integer variable
x(v,r) variable indicating how many times each available ship must sail each route
;

binary variable
y(p) 0-1 variable indicating that port p is serviced 
u(v) 0-1 variable indicating that ship v is used
;

free variable
z objective value 
;

*I define the objective function and equations
equations
obj objective function which has to minimize total yearly cost
numPorts the company has to service at least k ports
shipYear(v) the number of days each ship spends sailing must be less than or equal to the number of days each ship can sail per year
minPorts(p) the company has to visit port p at least D(p) times if they choose to visit port p
incompatiblePortPairs1 the company can visit at most one of the ports Singapore and Osaka
incompatiblePortPairs2  the company can visit at most one of the ports Incheon and Victoria
;

obj .. z =e= sum((v,r), C(v,r)*x(v,r))+sum(v,F(v)*u(v));

numPorts.. sum(p,y(p)) =g= k;

shipYear(v).. sum(r, x(v,r)*T(v,r)) =l= G(v)*u(v);

minPorts(p).. sum((v,r),A(p,r)*x(v,r)) =g= D(p)*y(p);

incompatiblePortPairs1.. sum(p$h1(p),y(p)) =l= 1;

incompatiblePortPairs2.. sum(p$h2(p),y(p)) =l= 1;

*option limrow = 1000;

*I solve the problem
model fleetPlanning /all/;
solve fleetPlanning using mip MINIMIZING z;
display x.l, y.l, u.l, z.l;


