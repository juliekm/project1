*Key Management in Wireless Sensor Networks*
set
n set of nodes /n1*n8/
k set of keys /k1*k30/
;

alias(n,i,j)

parameter
T how many times each key must be used at most
q how many keys a pair of nodes must share in order to get a direct connection 
;

T=2;
q=3;

*I define the variables 
free variables
z objective value
;

binary variables
x(k,n) 0-1 variable indicating that key k is assigned to node n
y(i,j) 0-1 variable indicating that there is a direct connection between node i and node j
w(i,j,k) 0-1 variable indicating if node i and node j share key k
;

*I define the objective function and equations
equations
obj objective function which maximizes the number of direct connections between the nodes
minKeys(i,j) if two nodes have at least three keys in common there is a direct connection
limitMemory(n) the keys in a given node can not exceed the memory limit for the node 
limitKeys(k) each key must be used at most T times
constraint4(i,j,k) if a key is assigned to two different nodes the variable w(i,j,k) can be equal to 1 or 0
constr5(i,j,k) if a key is assigned to two different nodes the variable w(i,j,k) has to be equal to 1 else it can be both 0 or 1
;

obj .. z =e= sum((i,j)$(ord(i)<>ord(j) and ord(i)<ord(j)), y(i,j));
minKeys(i,j)$(ord(i)<>ord(j) and ord(i)<ord(j)).. sum(k,w(i,j,k)) =g= q*y(i,j);
limitMemory(n).. sum(k,x(k,n)*186) =l= 1000;
limitKeys(k).. sum(n,x(k,n)) =l= T;
constr4(i,j,k)$(ord(i)<>ord(j) and ord(i)<ord(j)).. x(k,i)+x(k,j) =g= 2*w(i,j,k);
constr5(i,j,k)$(ord(i)<>ord(j) and ord(i)<ord(j)).. x(k,i)+x(k,j) =l= 1+w(i,j,k);
*The last two constraints ensures that if two different nodes share a key, the variable w(i,j,k) is equal to 1
*In some of the constraints there is a if-statement (given by $ in GAMS), which makes sure that no connections are made between the same index and thus not counted multiple times in the objective value 

*I solve the problem
model keyManagement /all/;
solve keyManagement using mip maximizing z;
display x.l, y.l, w.l, z.l;
