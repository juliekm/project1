*Problem Formation*

set
k set of formations /442, 352, 4312, 433, 343, 4321/
j set of player-roles /GK, CDF, LB, RB, CMF, LW, RW, OMF, CFW, SFW/
i set of players /P1*P25/
Q(i) subset of quality players /P13, P20, P21, P22/
S(i) subset of strength players /P10, P12, P23/
;

table c(i,j) fitness of assigning player i to player-role j
      GK CDF  LB  RB CMF  LW  RW OMF CFW SFW
P1    10   0   0   0   0   0   0   0   0   0
P2     9   0   0   0   0   0   0   0   0   0
P3   8.5   0   0   0   0   0   0   0   0   0
P4     0   8   6   5   4   2   2   0   0   0
P5     0   9   7   3   2   0   2   0   0   0
P6     0   8   7   7   3   2   2   0   0   0
P7     0   6   8   8   0   6   6   0   0   0
P8     0   4   5   9   0   6   6   0   0   0
P9     0   5   9   4   0   7   2   0   0   0
P10    0   4   2   2   9   2   2   0   0   0
P11    0   3   1   1   8   1   1   4   0   0
P12    0   3   0   2  10   1   1   0   0   0
P13    0   0   0   0   7   0   0  10   6   0
P14    0   0   0   0   4   8   6   5   0   0
P15    0   0   0   0   4   6   9   6   0   0
P16    0   0   0   0   0   7   3   0   0   0
P17    0   0   0   0   3   0   9   0   0   0
P18    0   0   0   0   0   0   0   6   9   6
P19    0   0   0   0   0   0   0   5   8   7
P20    0   0   0   0   0   0   0   4   4  10
P21    0   0   0   0   0   0   0   3   9   9
P22    0   0   0   0   0   0   0   0   8   8
P23    0   3   1   1   8   4   3   5   0   0
P24    0   3   2   4   7   6   5   6   4   0
P25    0   4   2   2   6   7   5   2   2   0
;

table a(k,j) number of player-role j needed in formation k
      GK CDF  LB  RB CMF  LW  RW OMF CFW SFW
442    1   2   1   1   2   1   1   0   2   0
352    1   3   0   0   3   1   1   0   1   1
4312   1   2   1   1   3   0   0   1   2   0
433    1   2   1   1   3   0   0   0   1   2
343    1   3   0   0   2   1   1   0   1   2
4321   1   2   1   1   3   0   0   2   1   0
;

scalar
np number of players in one formation
;
np=11

*I define the variables 
free variables
z objective value
;

binary variables
x(i,j) 0-1 variable indicating that player i is assigned to player-role j
y(k) 0-1 variable indicating which formation the coach choose
delta(i) 0-1 variable indicating whether player i is selected
;

*I define the objective function and equations
equations
obj objective function which maximizing the total fitness player-role
formation exactly one formation must be chosen
numPlayer(j) number of players must be equal to what the different formation require
maxPlayer(i) one player can play a maximum of one player-role
quality the coach has to employ at least one quality player 
strength if all quality players are employed at least one strength player is employed
;

obj .. z =e= sum((i,j), c(i,j)*x(i,j));
formation.. sum(k,y(k)) =e= 1;
numPlayer(j).. sum(i, x(i,j)) =e= sum(k,a(k,j)*y(k));
maxPlayer(i).. sum(j,x(i,j)) =l= 1;
quality.. sum(i$Q(i),delta(i)) =g= 1;
strength.. sum(i$Q(i),delta(i)) =l= sum(i$S(i),delta(i)) +3;

*I solve the problem
model problemFormation /all/;
solve problemFormation using mip maximizing z;
display x.l, y.l, z.l;
